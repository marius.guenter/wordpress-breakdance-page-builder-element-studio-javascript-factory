The JavaScript files are natively included in WordPress (enqueue_script).

```
add_action( 'wp_enqueue_scripts', function () {
    wp_enqueue_script( 'bd-factory', 'YOUR_PATH/dom-util.js', [], false, false );
    wp_enqueue_script( 'bd-factory', 'YOUR_PATH/dom.js', [], false, false );
    wp_enqueue_script( 'bd-factory', 'YOUR_PATH/bd-factory.js', [], false, false );
    wp_enqueue_script( 'bd-factory-builder', 'YOUR_PATH/bd-factory-builder.js', [], false, false );
});

```

Next, open Element Studio and create or open your custom element.
Add a dependency for this element. In the Dependencies Tab for this element, reference your custom JS file.
In the Properties Tab of that element, reference the Selector to your JS Class in "on mounted element":

```
window.BD_FACTORY_BUILDER.add('%%SELECTOR%%', window.YOUR_CUSTOM_JS_CLASS);
```

In the "on property change" field, you need to add:

```
window.BD_FACTORY_BUILDER.get('%%SELECTOR%%').set();
```

You can use any function you defined (instead of "set") from your custom JS Class.

For your custom JS File look at: example--js-for-your-custom-element.js

That's it. You're all set!<br>
Now, your JavaScript logic will be reset with every change in the Breakdance Page Builder.<br>
In the Frontend your custom js class is initialized on document ready or a custom event (like loaded, if it is needed).

