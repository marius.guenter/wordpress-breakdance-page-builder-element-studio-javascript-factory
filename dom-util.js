window.DOM_UTIL_CLASS = class DOM_UTIL {

    static get_closest_element( element, class_name ) {

        if( element.classList.contains( class_name ) ) {
            return element;
        }
        if( element.parentNode.nodeName !== 'BODY' ) {
            return window.DOM_UTIL_CLASS.get_closest_element( element.parentNode, class_name );
        }
        return null;

    }

    static get_css_variable( variable) {

        return getComputedStyle( document.body ).getPropertyValue( variable );

    }

    static get_element_offset( element ) {

        let top = 0;
        let left = 0;
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);
        return { top, left };

    }

    static get_html_element() {

        return document.getElementsByTagName('html')[0];

    }

}
