class BD_FACTORY_BUILDER_CLASS {

    constructor() {

        this.isBuilder = !!window.parent.Breakdance;

        if( this.isBuilder ) {
            this.elements = {};
            let d_html = document.getElementsByTagName('html')[0];
            d_html.classList.add( 'bd-is-builder' );
        }

    }

    /* 
     * ads JS Class to Selector for that Element
     * selector = CSS Class of Element
     * cl = JS Class of Element
     */
    add( selector, cl ) {

        if( this.isBuilder ) {
            let d_element = document.querySelectorAll( selector );
            if ( d_element.length > 0 ) {
                this.elements[ selector ] = new cl( d_element[ 0 ] );
            }
        }

    }

    get( selector ) {

        return this.elements[ selector ];

    }

    remove( selector ) {

        delete this.elements[ selector ];

    }

}

window.BD_FACTORY_BUILDER = new GBD_FACTORY_BUILDER_CLASS();
