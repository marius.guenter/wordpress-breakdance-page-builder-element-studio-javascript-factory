window.BD_FACTORY_CLASS = class BD_FACTORY {
    
    constructor() {

        this.isBuilder = !!window.parent.Breakdance;
        this.elements = [];

    }

    /* 
     * cl = JS Class of Element
     * slug = CSS Class of Element
     * event = Init Event
     */
    add( cl, slug, event = window.DOM_CLASS.EVENT_DOM_READY ) {

        if ( !this.isBuilder ) {
            this.elements.push( new BD_FACTORY_ELEMENT( cl, slug, event ) );
        }

    }
    
}

class BD_FACTORY_ELEMENT {
    
    /* 
     * cl = JS Class of Element
     * slug = CSS Class of Element
     * event = Init Event
     */
    constructor( cl, slug, event ) {

        this.slug = slug;
        this.cl = cl;
        this.event = event;
        this.elements = [];
        window.addEventListener( event, this.init.bind( this ) );
        this.init();

    }

    init() {

        // Selects all Elements with class "this.slug" and add 'bd-element-loaded' class so thiese elements are not loaded twice
        let d_elements = document.querySelectorAll( '.' + this.slug + ':not(.bd-element-loaded)' );
        for( let d_element of d_elements ) {
            d_element.classList.add( 'bd-element-loaded' );
            this.elements.push( new this.cl( d_element ) );
        }

    }

}

window.BD_ELEMENT_FACTORY = new window.BD_FACTORY_CLASS();
