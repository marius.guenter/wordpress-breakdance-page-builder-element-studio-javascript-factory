window.BD_ELEMENT_HELLO_WORLD = class BD_ELEMENT_HELLO_WORLD {

    constructor( d_root ) {

        // Root Dom Node of that Custom BD Element
        this.d_root = d_root;

        // Event Listener
        this.el_click = this.message.bind( this );
        this.set();

    }

    set() {

        // Removes all Listeneres and so on (every property change in Breakdance Builder - Backend)
        this.reset();

        // Add Listener
        this.d_root.addEventListener( 'click', this.el_click );

    }

    reset() {

        // Remove Listener
        this.d_root.removeEventListener( 'click', this.el_click );

    }

    message() {

        console.log( "HELLO WORLD" );

    }

}

/*
 * 1 Parameter: Referenced JS Class
 * 2 Parameter: Defined root CSS Class for that Custom BD Element
 */
window.BD_ELEMENT_FACTORY.add( window.BD_ELEMENT_HELLO_WORLD, 'bd-element-hello-world' );
