window.DOM_CLASS = class DOM {

    static EVENT_DOM_READY = 'event.dom.ready';
    static EVENT_DOM_READY_INIT = 'event.dom.ready.init';
    static EVENT_DOM_LOAD = 'event.dom.load';
    static EVENT_DOM_LOAD_INIT = 'event.dom.load.init';
    static EVENT_DOM_RESIZE = 'event.dom.resize';
    static EVENT_DOM_ORIENTATIONCHANGE = 'event.dom.orientationchange';

    constructor() {

        this.init();

    }

    init() {

        if (document.readyState !== 'loading') {
            this.trigger_ready();
        } else {
            document.addEventListener( 'DOMContentLoaded', () => this.trigger_ready() );
        }
        window.addEventListener( 'load', () => this.trigger_load() );
        window.addEventListener( 'resize', () => this.trigger_resize() );
        window.addEventListener( 'orientationchange', () => this.trigger_orientation_change() );

    }

    trigger_ready() {

        let d_html = document.getElementsByTagName( 'html' )[0];
        d_html.classList.add( 'dom-ready' );
        let event = new Event( window.DOM_CLASS.EVENT_DOM_READY, { bubbles: false, cancelable: true } );
        window.dispatchEvent(event);
        setTimeout( () => {
            let event = new Event( window.DOM_CLASS.EVENT_DOM_READY_INIT, { bubbles: false, cancelable: true } );
            window.dispatchEvent( event );
            d_html.classList.add( 'dom-ready-init' );
        }, 1 );

    }

    trigger_load() {

        let d_html = document.getElementsByTagName( 'html' )[0];
        d_html.classList.add( 'dom-load' );
        let event = new Event( window.DOM_CLASS.EVENT_DOM_LOAD, { bubbles: false, cancelable: true } ) ;
        window.dispatchEvent( event );
        setTimeout( () => {
            let event = new Event( window.DOM_CLASS.EVENT_DOM_LOAD_INIT, { bubbles: false, cancelable: true } );
            window.dispatchEvent( event );
            d_html.classList.add( 'dom-load-init' );
        }, 1 );

    }

    trigger_resize() {

        let event = new Event( window.DOM_CLASS.EVENT_DOM_RESIZE, { bubbles: false, cancelable: true } );
        window.dispatchEvent( event );

    }

    trigger_orientation_change() {

        let event = new Event( window.DOM_CLASS.EVENT_DOM_ORIENTATIONCHANGE, { bubbles: false, cancelable: true } );
        window.dispatchEvent( event );

    }

}

window.DOM = new DOM_CLASS();
